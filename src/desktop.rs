use std::{os::raw::c_uint, ptr};
use x11::{xlib, xtest};
use xlib::Display;

#[derive(Debug, Copy, Clone)]
pub enum State {
    Down,
    Up,
}

#[derive(Debug, Copy, Clone)]
pub enum ButtonType {
    Left,
    Middle,
    Right,
}

#[derive(Debug, Copy, Clone)]
pub enum ScrollDirection {
    Up,
    Down,
    Left,
    Right,
}

pub struct Desktop {
    display: *mut Display,
}

impl Desktop {
    pub fn new() -> Self {
        unsafe {
            let display = xlib::XOpenDisplay(ptr::null());
            if display.is_null() {
                panic!("Could not open Display");
            }

            Desktop { display }
        }
    }

    pub fn move_cursor_relative(&self, dx: i32, dy: i32) {
        unsafe {
            xtest::XTestFakeRelativeMotionEvent(self.display, dx, dy, xlib::CurrentTime);
            xlib::XFlush(self.display);
        }
    }

    pub fn scroll(&self, direction: ScrollDirection) {
        let button = match direction {
            ScrollDirection::Up => 4,
            ScrollDirection::Down => 5,
            ScrollDirection::Left => 6,
            ScrollDirection::Right => 7,
        };

        unsafe {
            xtest::XTestFakeButtonEvent(self.display, button, 1, xlib::CurrentTime);
            xtest::XTestFakeButtonEvent(self.display, button, 0, xlib::CurrentTime);
            xlib::XFlush(self.display);
        }
    }

    pub fn button(&self, state: State, button_type: ButtonType) {
        let down = match state {
            State::Down => 1,
            State::Up => 0,
        };

        let button = match button_type {
            ButtonType::Left => 1,
            ButtonType::Middle => 2,
            ButtonType::Right => 3,
        };

        unsafe {
            xtest::XTestFakeButtonEvent(self.display, button, down, xlib::CurrentTime);
            xlib::XFlush(self.display);
        }
    }

    pub fn key(&self, state: State, keysym: c_uint) {
        let down = match state {
            State::Down => 1,
            State::Up => 0,
        };

        unsafe {
            let keycode = xlib::XKeysymToKeycode(self.display, keysym as _);
            xtest::XTestFakeKeyEvent(self.display, keycode as _, down, xlib::CurrentTime);
            xlib::XFlush(self.display);
        }
    }
}

impl Drop for Desktop {
    fn drop(&mut self) {
        unsafe {
            xlib::XFlush(self.display);
            xlib::XCloseDisplay(self.display);
        }
    }
}
