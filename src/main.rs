mod desktop;

use desktop::{ButtonType, Desktop, ScrollDirection, State};
use sdl2::{event::Event, joystick::HatState};
use std::{
    os::raw::c_uint,
    thread,
    time::{Duration, Instant},
};
use x11::keysym::*;

const MOUSE_HORIZONTAL_MULTIPLIER: f32 = 150.0;

const MOUSE_VERTICAL_DEADZONE: f32 = 0.1;
const MOUSE_VERTICAL_EXPONENT: f32 = 2.0;
const MOUSE_VERTICAL_MULTIPLIER: f32 = 25.0;

// -1.0 = fully up, 1.0 = fully down
const W_THRESHOLD: f32 = -0.2;

// lower is faster
const SCROLL_SPEED: u64 = 5;

const BUTTON_MAPS: &[ButtonMap] = &[
    // Circle
    ButtonMap {
        button_idx: 4,
        action: Action::Key(XK_Return),
    },
    // L2
    ButtonMap {
        button_idx: 9,
        action: Action::Key(XK_Escape),
    },
    // R2
    ButtonMap {
        button_idx: 8,
        action: Action::Key(XK_space),
    },
    // SE
    ButtonMap {
        button_idx: 6,
        action: Action::Key(XK_Super_L),
    },
    // ST
    ButtonMap {
        button_idx: 7,
        action: Action::Button(ButtonType::Middle),
    },
    // Shifter (Right)
    ButtonMap {
        button_idx: 1,
        action: Action::Button(ButtonType::Right),
    },
    // Shifter (Left)
    ButtonMap {
        button_idx: 0,
        action: Action::Button(ButtonType::Left),
    },
];

#[derive(Copy, Clone)]
enum Action {
    Key(c_uint),
    Button(ButtonType),
}

struct ButtonMap {
    button_idx: u8,
    action: Action,
}

fn main() {
    let desktop = Desktop::new();

    let sdl2 = sdl2::init().unwrap();
    let joystick = sdl2.joystick().unwrap();
    assert_eq!(joystick.num_joysticks().unwrap(), 1);
    let t150 = joystick.open(0).unwrap();
    assert_eq!(t150.name(), "Thrustmaster Thrustmaster T150RS");

    let mut wheel = 0.0;
    let mut gas_pedal = -1.0;
    let mut break_pedal = -1.0;

    let mut scroll_direction = None;
    let mut last_scroll_frame = 0;
    let mut w_was_down = false;

    let mut event_pump = sdl2.event_pump().unwrap();
    let mut last = Instant::now();
    let mut enabled = true;
    'main: for i in 0u64.. {
        // loop control
        let elapsed = last.elapsed();
        let interval = Duration::from_secs_f64(1.0 / 60.0);
        let sleep = interval
            .checked_sub(elapsed)
            .unwrap_or_else(|| Duration::default());
        thread::sleep(sleep);
        last = Instant::now();

        for event in event_pump.poll_iter() {
            match event {
                Event::JoyDeviceAdded { .. } => (),
                Event::Quit { .. } | Event::JoyDeviceRemoved { .. } => break 'main,
                Event::JoyButtonDown { button_idx: 10, .. } => {
                    enabled = !enabled;
                    println!("[i] Enabled: {}", enabled);
                }
                _ if !enabled => continue,
                Event::JoyAxisMotion {
                    axis_idx: 0, value, ..
                } => wheel = (value as f32 / i16::MAX as f32).max(-1.0),
                Event::JoyAxisMotion {
                    axis_idx: 1, value, ..
                } => break_pedal = (-(value as f32) / i16::MAX as f32).min(1.0),
                Event::JoyAxisMotion {
                    axis_idx: 2, value, ..
                } => gas_pedal = (-(value as f32) / i16::MAX as f32).min(1.0),
                Event::JoyButtonDown { button_idx, .. } | Event::JoyButtonUp { button_idx, .. } => {
                    let state = match event {
                        Event::JoyButtonDown { .. } => State::Down,
                        Event::JoyButtonUp { .. } => State::Up,
                        _ => unreachable!(),
                    };

                    if let Some(map) = BUTTON_MAPS.iter().find(|map| map.button_idx == button_idx) {
                        match &map.action {
                            &Action::Key(keysym) => desktop.key(state, keysym),
                            &Action::Button(button) => desktop.button(state, button),
                        }
                    } else if let State::Down = state {
                        println!("[w] Unmapped button pressed ({})", button_idx)
                    }
                }
                Event::JoyHatMotion { state, .. } => {
                    match state {
                        HatState::Up => scroll_direction = Some(ScrollDirection::Up),
                        HatState::Down => scroll_direction = Some(ScrollDirection::Down),
                        HatState::Left => scroll_direction = Some(ScrollDirection::Left),
                        HatState::Right => scroll_direction = Some(ScrollDirection::Right),
                        _ => scroll_direction = None,
                    }

                    if scroll_direction.is_some() {
                        last_scroll_frame = i;
                    }
                }
                event => panic!("unmapped event: {:?}", event),
            }
        }

        if !enabled {
            continue;
        }

        // mouse movement
        let dx = wheel * MOUSE_HORIZONTAL_MULTIPLIER;
        let dy = (gas_pedal.abs() - MOUSE_VERTICAL_DEADZONE)
            .max(0.0)
            .powf(MOUSE_VERTICAL_EXPONENT)
            * gas_pedal.signum()
            * MOUSE_VERTICAL_MULTIPLIER;
        desktop.move_cursor_relative(dx.round() as _, dy.round() as _);

        // pressing w
        let w_down = break_pedal >= W_THRESHOLD;
        if w_down != w_was_down {
            desktop.key(if w_down { State::Down } else { State::Up }, XK_w);
            w_was_down = w_down;
        }

        // scrolling
        if let Some(direction) = &scroll_direction {
            if (i - last_scroll_frame) % SCROLL_SPEED == 0 {
                desktop.scroll(*direction);
            }
        }
    }
}
