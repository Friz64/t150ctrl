# t150ctrl

Lets you control games and your desktop with your Thrustmaster T150 steering wheel

## Mapping

| Wheel            | Desktop                   |
| ---------------- | ------------------------- |
| Steering         | Horizontal mouse movement |
| Gas Pedal        | Vertical mouse movement   |
| Brake Pedal      | Presses W                 |
| D-Pad            | Scrolling                 |
| Down/Up Shifters | Left/Right click          |
| L2 Button        | Escape                    |
| SE Button        | Super key                 |
| ST Button        | Middle click              |
| R2 Button        | Space                     |
| Circle Button    | Enter                     |
| L3 Button        | (Pauses/Unpauses program) |
